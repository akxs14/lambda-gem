# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'version'

Gem::Specification.new do |gem|
  gem.name          = "lambda_gem"
  gem.version       = LambdaGem::VERSION
  gem.authors       = ["angelos"]
  gem.email         = ["angelos.kapsimanis@gmail.com"]
  gem.description   = "Lambda expressions support"
  gem.summary       = "The current gem provides a lambda 
                      calculus implementation with the possibility
                      to define any arbitrary operator with any
                      functionality"
  gem.homepage      = ""

  gem.files         = `git ls-files`.split($/)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ["lib"]

  gem.add_development_dependency "rspec", "~> 2.6"
end
