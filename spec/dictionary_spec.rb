require 'lambda_gem'

include LambdaGem

describe Dictionary do

	it "the default dictionary should contain seven operators" do
		Dictionary.new.count.should eq(7)
	end

	it "should return nil for an unknown operator" do
		dict = Dictionary.new
		dict["boing"].should be_nil
	end

	it "an instance should return the correct lambda" do
		dict = Dictionary.new
		a, b, and_lambda = true, false, lambda { |a,b| a and b }

		result_dict = dict["and"].call(a, b)
		result_local = and_lambda.call(a, b)
		result_local.should eq(result_dict)
	end

	it "should use the user defined dictionary if given in initialization" do
		userDict = Hash.new 
		userDict["xy"] = lambda { |a,b| a**b }

		dict = Dictionary.new(userDict)
		dict["xy"].call(2,3).should eq(8)
	end

end