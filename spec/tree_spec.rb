require 'lambda_gem'

include LambdaGem

describe Tree do 
	it "the root should a node with nill value an empty tree" do
		Tree.new.root.data.should be_nil and Tree.new.root.class.should eq(Node)
	end

	it "the root should be of a node class" do
		Tree.new(1).root.class.should eq(Node)
	end

	it "every node should contain the data given to it" do
		tree = Tree.new(1)
		tree.root.data.should eq(1)
	end
end