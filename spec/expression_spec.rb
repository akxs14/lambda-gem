require 'lambda_gem'

include LambdaGem

describe Expression do 
	it "should have a nil expression if nothing is given" do
		Expression.new().formula.should be_nil
	end
	
	it "should have an infix order as default" do
		Expression.new().current_order.should eq(:infix)
	end

	it "should have infix ordering in case a non-acceotable ordering was passed" do
		Expression.new(nil,:noorder).current_order.should eq(:infix)
	end

	it "should contain a Tree class instance in @tree" do
		Expression.new().tree.class.should eq(Tree)
	end

	it "should verify correctly if a node's data are an operator contained in a given dictionary" do
		expression = Expression.new
		expression.operator?('+').should be_true
	end

	it "should not recognize a number as an operator" do
		expression = Expression.new
		expression.operator?('3').should be_false
	end	

	it "should be able to verify operators from a node" do
		expression, node = Expression.new, Node.new(1)
		expression.operator?(node).should be_false
	end

	it "should be able to store the expression as a expression tree and traverse it back to its original form" do
		#the expression could also be given as 
		# => (nd_age = 1 or nd_age = 2) and nd_gnd = 2
		# it would be parsed correctly 
		# but the returned expression couldn't match because of expressions
		expression_test = "(((nd_age = 1) or (nd_age = 2)) and (nd_gnd = 2))"
		expression = Expression.new expression_test, :infix
		expression.tree.traverse(:infix).should eq(expression_test)
	end

	it "should identify correctly an operator " do
		Expression.new.operator?('+').should be_true
	end

	it "should not identify a symbol as an operator " do
		Expression.new.operator?('ni_auto').should be_false
	end

	it "should not identify a digit as an operator " do
		Expression.new.operator?('9').should be_false
	end	

	it "should be able to correctly return all the token orders of an expression" do
		expr_inorder = "(1 + 1)"
		expr_preorder = "(+ 1 1)"
		expr_postorder = "(1 1 +)"
		expr = Expression.new "(1 + 1)"

		expr.tree.traverse(:prefix).should eq(expr_preorder) and
		expr.tree.traverse(:postfix).should eq(expr_postorder) and
		expr.tree.traverse(:infix).should eq(expr_inorder)		
	end
end
