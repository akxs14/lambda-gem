require 'lambda_gem'

include LambdaGem

describe Node do
	it "an empty node is a leaf" do
		Node.new.leaf?
	end

	it "purge is called, all data in the node must be cleared" do
		node = Node.new(1)
		node.purge
		node.data.should be_nil and node.left.should be_nil and node.right.should be_nil
	end
end
