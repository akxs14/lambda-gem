require_relative 'lib/tree'
require_relative 'lib/expression'

include LambdaGem

tree = Tree.new '+'
tree.root.left = Node.new(1)
tree.root.right = Node.new(1)

# puts "root #{tree.root.data}"
# puts "left: #{tree.root.left.data}"
# puts "right: #{tree.root.right.data}"

exp = Expression.new 

exp.formula = "((nd_gnd = 1) AND 
		(nd_agr = 1 OR nd_agr = 2 OR nd_agr = 3 OR nd_agr = 4)) AND
		((ni_care = 1 OR ni_care = 2 OR ni_care = 3 OR ni_care = 4) AND 
		(ni_perf = 1 OR ni_perf = 2 OR ni_perf = 3 OR ni_perf = 4)) AND 
		((na_book = 1 OR na_book = 2))"
puts exp.tree.traverse :infix

exp.formula = "(nd_agr = 1 or nd_agr = 0) and ni_auto = 9"
puts exp.evaluate
