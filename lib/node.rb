# Description
#   A tree structure that contains the expression 
#   in an infix notation tree.
module LambdaGem

  class Node
    attr_accessor :left, :right, :data

    def initialize data=nil
      @data, @left, @right = data, nil, nil
    end

    def leaf?
      @left.nil? and @right.nil?
    end

    def purge 
      @data, @left, @right = nil, nil, nil
    end

    def traverse order=:infix
      if leaf?
        @data
      else
        left_child, right_child = @left.traverse(order), @right.traverse(order)

        strs = case order
          when :prefix then [@data, left_child, right_child]
          when :infix then [left_child, @data, right_child]
          when :postfix then [left_child, right_child, @data]
          else []
        end   
        "(" + strs.join(" ") + ")"
      end             
    end

  end
end