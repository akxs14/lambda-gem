require_relative 'node'

module LambdaGem
  class Tree
    attr_accessor :root

    def initialize root=nil
        @root = Node.new root
    end

    def preorder_insert new_data
      new_data
    end

    def traverse order=:infix
      @root.traverse order
    end

  end
end