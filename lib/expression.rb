require_relative 'tree'
require_relative 'dictionary'

module LambdaGem
  class Expression
    attr_accessor :tree, :current_order, :dictionary

    def initialize expression=nil, order=:infix, dictionary=nil     
      order = :infix if !@@AVAILABLE_ORDERING.include? order
      @dictionary = Dictionary.new
      @formula, @current_order, @tree = expression, order, Tree.new
      tree_from_expression(expression,order) if !expression.nil?
    end

    def formula
     @formula
    end

    def formula= new_expression
      tree_from_expression new_expression
      @formula = new_expression
    end

    def tree_from_expression expression=@formula, order=:infix
      tokens = tokenize expression
      operator_stack, node_stack = [], []

      tokens.each do |token|
        token = token.downcase

        if operator?(token)
          until(operator_stack.empty? or
                operator_stack.last.data == "(" or
                @dictionary.operator_priority[operator_stack.last.data] < @dictionary.operator_priority[token])
            pop_connect_push operator_stack, node_stack
          end
          operator_stack.push(Node.new(token))
        elsif token == "("
          operator_stack.push(Node.new(token))
        elsif token == ")"
          while operator_stack.last.data != "("
            pop_connect_push operator_stack, node_stack
          end
          operator_stack.pop #throw '('
        else
          node_stack.push(Node.new(token))
        end

      end

      until operator_stack.empty?
        pop_connect_push operator_stack, node_stack
      end

      @tree.root = node_stack.last
      @tree
    end

    def operator?(token)
     @dictionary.contains?(token.to_s)
    end

    def evaluate
     eval(@tree.root)
    end

    private
      @@AVAILABLE_ORDERING = [:prefix,:infix,:postfix]

      def eval node
        if !node.leaf? and operator?(node.data)
          @dictionary[node.data].call( eval(node.left), eval(node.right) )
        else
          #if a number, cast from string to numeric, otherwise return it as-is
          (/[0-9]/ =~ node.data) ? node.data.to_f : node.data
        end
      end

      def tokenize expression
        expression.gsub('(', ' ( ').gsub(')', ' ) ').split(' ')
      end

      #pops the topmost node, assigns the next two nodes
      #as its left and right branches and is pushed back
      def pop_connect_push operator_stack, node_stack
        temp = operator_stack.pop
        temp.right = node_stack.pop
        temp.left = node_stack.pop
        node_stack.push temp
      end
  end
end

