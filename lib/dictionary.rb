# Description:
#   A dictionary structure containing the functionality
#   of the available operators. 
module LambdaGem

  class Dictionary
    attr_reader :operator_priority

    def initialize dict=nil
      if dict.nil?
        @operator_dict, @operator_priority = Hash.new, Hash.new
        @operator_priority.default(-1)
        populate_default_dict
        populate_default_priorities
      else
        @operator_dict = dict
      end
    end

    def count
      @operator_dict.count
    end

    def [] operator
      @operator_dict[operator]
    end

    def contains? operator
      @operator_dict.include? operator
    end

    def add_operator symbol, block, priority
      @operator_dict[symbol] = block
      @operator_priority[symbol] = priority
    end

    private
      def populate_default_dict
        @operator_dict["="] = lambda { |a, b| a = b }        
        @operator_dict["and"] = lambda { |a,b| a and b }
        @operator_dict["or"]  = lambda { |a,b| a or b }
        @operator_dict["+"]  = lambda { |a,b| a + b }
        @operator_dict["-"]  = lambda { |a,b| a - b }
        @operator_dict["*"]  = lambda { |a,b| a * b }
        @operator_dict["/"]  = lambda { |a,b| a / b }       
      end

      def populate_default_priorities
        @operator_priority["and"] = 1
        @operator_priority["or"]  = 1
        @operator_priority["+"]  = 1
        @operator_priority["-"]  = 1
        @operator_priority["*"]  = 2
        @operator_priority["/"]  = 2
        @operator_priority["="]  = 3        
      end
  end

end
